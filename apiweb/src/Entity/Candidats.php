<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\CandidatsRepository")
 */
class Candidats
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $civilite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $datenaissance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $situationfamiliale;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $profil;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $disponibilite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dateintegration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $heureintegration;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $grade;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numportable;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numfixe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $zonemobilite;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $datecreation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $heurecreation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etatdossier;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ajouterpar;

    /**
     * @ORM\Column(type="text")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $login;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getCin(): ?string
    {
        return $this->cin;
    }

    public function setCin(string $cin): self
    {
        $this->cin = $cin;

        return $this;
    }

    public function getDatenaissance(): ?string
    {
        return $this->datenaissance;
    }

    public function setDatenaissance(string $datenaissance): self
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    public function getSituationfamiliale(): ?string
    {
        return $this->situationfamiliale;
    }

    public function setSituationfamiliale(string $situationfamiliale): self
    {
        $this->situationfamiliale = $situationfamiliale;

        return $this;
    }

    public function getProfil(): ?string
    {
        return $this->profil;
    }

    public function setProfil(string $profil): self
    {
        $this->profil = $profil;

        return $this;
    }

    public function getDisponibilite(): ?string
    {
        return $this->disponibilite;
    }

    public function setDisponibilite(string $disponibilite): self
    {
        $this->disponibilite = $disponibilite;

        return $this;
    }

    public function getDateintegration(): ?string
    {
        return $this->dateintegration;
    }

    public function setDateintegration(string $dateintegration): self
    {
        $this->dateintegration = $dateintegration;

        return $this;
    }

    public function getHeureintegration(): ?string
    {
        return $this->heureintegration;
    }

    public function setHeureintegration(string $heureintegration): self
    {
        $this->heureintegration = $heureintegration;

        return $this;
    }

    public function getGrade(): ?string
    {
        return $this->grade;
    }

    public function setGrade(string $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getNumportable(): ?string
    {
        return $this->numportable;
    }

    public function setNumportable(string $numportable): self
    {
        $this->numportable = $numportable;

        return $this;
    }

    public function getNumfixe(): ?string
    {
        return $this->numfixe;
    }

    public function setNumfixe(?string $numfixe): self
    {
        $this->numfixe = $numfixe;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getZonemobilite(): ?string
    {
        return $this->zonemobilite;
    }

    public function setZonemobilite(string $zonemobilite): self
    {
        $this->zonemobilite = $zonemobilite;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getDatecreation(): ?string
    {
        return $this->datecreation;
    }

    public function setDatecreation(string $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getHeurecreation(): ?string
    {
        return $this->heurecreation;
    }

    public function setHeurecreation(?string $heurecreation): self
    {
        $this->heurecreation = $heurecreation;

        return $this;
    }

    public function getEtatdossier(): ?string
    {
        return $this->etatdossier;
    }

    public function setEtatdossier(string $etatdossier): self
    {
        $this->etatdossier = $etatdossier;

        return $this;
    }

    public function getAjouterpar(): ?string
    {
        return $this->ajouterpar;
    }

    public function setAjouterpar(string $ajouterpar): self
    {
        $this->ajouterpar = $ajouterpar;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }
}
