<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DocumentsRepository")
 */
class Documents
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomdoc;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sourcedoc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $descdoc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $datedoc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $heuredoc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ajouterpardoc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $formatdoc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $icondoc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomdoc(): ?string
    {
        return $this->nomdoc;
    }

    public function setNomdoc(string $nomdoc): self
    {
        $this->nomdoc = $nomdoc;

        return $this;
    }

    public function getSourcedoc(): ?string
    {
        return $this->sourcedoc;
    }

    public function setSourcedoc(string $sourcedoc): self
    {
        $this->sourcedoc = $sourcedoc;

        return $this;
    }

    public function getDescdoc(): ?string
    {
        return $this->descdoc;
    }

    public function setDescdoc(?string $descdoc): self
    {
        $this->descdoc = $descdoc;

        return $this;
    }

    public function getDatedoc(): ?string
    {
        return $this->datedoc;
    }

    public function setDatedoc(?string $datedoc): self
    {
        $this->datedoc = $datedoc;

        return $this;
    }

    public function getHeuredoc(): ?string
    {
        return $this->heuredoc;
    }

    public function setHeuredoc(?string $heuredoc): self
    {
        $this->heuredoc = $heuredoc;

        return $this;
    }

    public function getAjouterpardoc(): ?string
    {
        return $this->ajouterpardoc;
    }

    public function setAjouterpardoc(?string $ajouterpardoc): self
    {
        $this->ajouterpardoc = $ajouterpardoc;

        return $this;
    }

    public function getFormatdoc(): ?string
    {
        return $this->formatdoc;
    }

    public function setFormatdoc(?string $formatdoc): self
    {
        $this->formatdoc = $formatdoc;

        return $this;
    }

    public function getIcondoc(): ?string
    {
        return $this->icondoc;
    }

    public function setIcondoc(?string $icondoc): self
    {
        $this->icondoc = $icondoc;

        return $this;
    }
}
