<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\HomeSliderRepository")
 */
class HomeSlider
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descritpion;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sourceimg;

     /**
     * @ORM\Column(type="string", length=255)
     */
    private $urldirect;   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre1(): ?string
    {
        return $this->titre1;
    }

    public function setTitre1(string $titre1): self
    {
        $this->titre1 = $titre1;

        return $this;
    }

    public function getTitre2(): ?string
    {
        return $this->titre2;
    }

    public function setTitre2(string $titre2): self
    {
        $this->titre2 = $titre2;

        return $this;
    }

    public function getDescritpion(): ?string
    {
        return $this->descritpion;
    }

    public function setDescritpion(string $descritpion): self
    {
        $this->descritpion = $descritpion;

        return $this;
    }

    public function getSourceimg(): ?string
    {
        return $this->sourceimg;
    }

    public function setSourceimg(string $sourceimg): self
    {
        $this->sourceimg = $sourceimg;

        return $this;
    }

    public function getUrldirect(): ?string
    {
        return $this->urldirect;
    }

    public function setUrldirect(string $urldirect): self
    {
        $this->urldirect = $urldirect;

        return $this;
    }    
}
