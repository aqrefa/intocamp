<?php

namespace App\Repository;

use App\Entity\Eauxsociaux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Eauxsociaux|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eauxsociaux|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eauxsociaux[]    findAll()
 * @method Eauxsociaux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EauxsociauxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Eauxsociaux::class);
    }

//    /**
//     * @return Eauxsociaux[] Returns an array of Eauxsociaux objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Eauxsociaux
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
