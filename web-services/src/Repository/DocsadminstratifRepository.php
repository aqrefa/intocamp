<?php

namespace App\Repository;

use App\Entity\Docsadminstratif;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Docsadminstratif|null find($id, $lockMode = null, $lockVersion = null)
 * @method Docsadminstratif|null findOneBy(array $criteria, array $orderBy = null)
 * @method Docsadminstratif[]    findAll()
 * @method Docsadminstratif[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocsadminstratifRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Docsadminstratif::class);
    }

//    /**
//     * @return Docsadminstratif[] Returns an array of Docsadminstratif objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Docsadminstratif
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
