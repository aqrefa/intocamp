import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocIntegrationComponent } from './doc-integration.component';

describe('DocIntegrationComponent', () => {
  let component: DocIntegrationComponent;
  let fixture: ComponentFixture<DocIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
