import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassoubliComponent } from './passoubli.component';

describe('PassoubliComponent', () => {
  let component: PassoubliComponent;
  let fixture: ComponentFixture<PassoubliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassoubliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassoubliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
