import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { BlokservicesService } from '../../service/blokservices.service';

@Component({
  selector: 'app-blok',
  templateUrl: './blok.component.html',
  styleUrls: ['./blok.component.css']
})
export class BlokComponent implements OnInit {

bloks;

////////////////////////Label du tableau//////////////////
ID = 'ID';
Adresse = 'Adresse';
Portable = 'Portable';
Fix = 'Fix';
Urlsiteweb = 'Url site web';
Email1 = 'Email 1';
Email2 = 'Email 2';

    constructor( 
	    private _http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private blokservice:BlokservicesService
    	) { }

	ngOnInit() {
		this.getALLInfo();
	}

///////////////////GetALL/////////////////////////
		  getALLInfo(){
		    this.blokservice.listInfos()
		    .subscribe(success=>{
		      this.bloks = success;
		    })
		  }
///////////////////FIN GetALL/////////////////////////

}

