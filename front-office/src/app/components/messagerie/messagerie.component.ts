import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.css']
})
export class MessagerieComponent implements OnInit {

    public sourcehref: string = "";

    constructor(private router: Router) {}

    ngOnInit() {
        this.sourcehref = this.router.url;
        //console.log(this.router.url);
    }

}
