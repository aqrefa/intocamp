import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrmIntegrationComponent } from './progrm-integration.component';

describe('ProgrmIntegrationComponent', () => {
  let component: ProgrmIntegrationComponent;
  let fixture: ComponentFixture<ProgrmIntegrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgrmIntegrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrmIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
