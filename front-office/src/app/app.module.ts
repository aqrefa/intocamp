import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule,HttpClient } from '@angular/common/http';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TopmenuComponent } from './components/topmenu/topmenu.component';
import { LoginComponent } from './components/login/login.component';
import { PassoubliComponent } from './components/passoubli/passoubli.component';
import { AcceuilComponent } from './components/acceuil/acceuil.component';
import { Page404Component } from './components/page404/page404.component';
import { DocIntegrationComponent } from './components/doc-integration/doc-integration.component';
import { CalendrierComponent } from './components/calendrier/calendrier.component';
import { ProgrmIntegrationComponent } from './components/progrm-integration/progrm-integration.component';
import { MessagerieComponent } from './components/messagerie/messagerie.component';
import { BlokComponent } from './components/blok/blok.component';
import { BlokservicesService } from './service/blokservices.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TopmenuComponent,
    LoginComponent,
    PassoubliComponent,
    AcceuilComponent,
    Page404Component,
    DocIntegrationComponent,
    CalendrierComponent,
    ProgrmIntegrationComponent,
    MessagerieComponent,
    BlokComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    RouterModule.forRoot([
      {path:"", component:AcceuilComponent, pathMatch: 'full'},
      {path:"login", component:LoginComponent},
      {path:"motdepassoubli", component:PassoubliComponent},
      {path:"dossierintegration", component:DocIntegrationComponent},
      {path:"calendrierintegration", component:CalendrierComponent},
      {path:"progmintegration", component:ProgrmIntegrationComponent},
      {path:"messagerie", component:MessagerieComponent},
      {path:"blok", component:BlokComponent},
      {path:"**", component:Page404Component}
      
    ])
  ],
  providers: [BlokservicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
