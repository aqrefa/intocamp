import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BlokservicesService {

  constructor(
    private _http:HttpClient
) { }

/*  
    On peut utiliser le typage pour les fonctions afin de retourner valeur de type observation
    comme listArticles():Observable<any>
*/

listInfos():Observable<any>{
   return this._http.get("http://127.0.0.1:8000/api/infocontacts/");
}

}


