import { TestBed, inject } from '@angular/core/testing';

import { BlokservicesService } from './blokservices.service';

describe('BlokservicesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlokservicesService]
    });
  });

  it('should be created', inject([BlokservicesService], (service: BlokservicesService) => {
    expect(service).toBeTruthy();
  }));
});
