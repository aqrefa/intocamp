import { TestBed, inject } from '@angular/core/testing';

import { DocumentsserviceService } from './documentsservice.service';

describe('DocumentsserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DocumentsserviceService]
    });
  });

  it('should be created', inject([DocumentsserviceService], (service: DocumentsserviceService) => {
    expect(service).toBeTruthy();
  }));
});
