import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class DocumentsserviceService {

    constructor(
    private _http:HttpClient
) { }

/* On peut utiliser le typage pour les fonctions afin de retourner valeur de type 
    observation comme listArticles():Observable<any> */

/**********Afficher tous les documents*******************/
listdesdocs():Observable<any>{
   return this._http.get("http://127.0.0.1:8000/api/documents/");
}

/**********Ajouter un document*******************/
postDocs(objet){
  return this._http.post('http://127.0.0.1:8000/api/documents/',objet);
}

/**********Récupérer un document by id*******************/
getDocs(id){
  return this._http.get('http://127.0.0.1:8000/api/documents/'+id);
}

/**********Supprimer un document by id*******************/
deleteDocs(id){
  return this._http.delete('http://127.0.0.1:8000/api/documents/'+id);
}

/**********Modifier un document by id*******************/
updateDocs(id,objet){
  return this._http.put('http://127.0.0.1:8000/api/documents/'+id,objet);
}



}
