import { TestBed, inject } from '@angular/core/testing';

import { CandidatsserviceService } from './candidatsservice.service';

describe('CandidatsserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CandidatsserviceService]
    });
  });

  it('should be created', inject([CandidatsserviceService], (service: CandidatsserviceService) => {
    expect(service).toBeTruthy();
  }));
});
