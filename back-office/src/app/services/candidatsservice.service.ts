import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CandidatsserviceService {

    constructor(
    private _http:HttpClient
) { }

/* On peut utiliser le typage pour les fonctions afin de retourner valeur de type 
    observation comme listArticles():Observable<any> */

/**********Afficher tous*******************/
listdescandidats():Observable<any>{
   return this._http.get("http://127.0.0.1:8000/api/candidats/");
}

/**********Ajouter un user*******************/
postUser(objet){
  return this._http.post('http://127.0.0.1:8000/api/candidats/',objet);
}

/**********Récupérer un user by id*******************/
getUser(id){
  return this._http.get('http://127.0.0.1:8000/api/candidats/'+id);
}

/**********Supprimer un user by id*******************/
deleteUser(id){
  return this._http.delete('http://127.0.0.1:8000/api/candidats/'+id);
}

/**********Modifier un user by id*******************/
updateUser(id,objet){
  return this._http.put('http://127.0.0.1:8000/api/candidats/'+id,objet);
}



}

