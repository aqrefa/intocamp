import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { DataTablesModule } from 'angular-datatables';


import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ErreurComponent } from './components/erreur/erreur.component';
import { ListesdescandidatsComponent } from './components/listesdescandidats/listesdescandidats.component';
import { AddcandidatsComponent } from './components/addcandidats/addcandidats.component';
import { UpdatecandidatsComponent } from './components/updatecandidats/updatecandidats.component';
import { ListesArchivescandidatsComponent } from './components/listes-archivescandidats/listes-archivescandidats.component';
import { AfficheruncandidatsComponent } from './components/afficheruncandidats/afficheruncandidats.component';
import { MenutopleftComponent } from './components/menutopleft/menutopleft.component';
import { LogoutComponent } from './components/logout/logout.component';
import { DocsaddComponent } from './components/docsadd/docsadd.component';
import { DocslisteComponent } from './components/docsliste/docsliste.component';
import { DocsdetailsComponent } from './components/docsdetails/docsdetails.component';
import { DocsupdateComponent } from './components/docsupdate/docsupdate.component';

import {CandidatsserviceService} from './services/candidatsservice.service';
import {DocumentsserviceService} from './services/documentsservice.service';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
    ErreurComponent,
    ListesdescandidatsComponent,
    AddcandidatsComponent,
    UpdatecandidatsComponent,
    ListesArchivescandidatsComponent,
    AfficheruncandidatsComponent,
    MenutopleftComponent,
    LogoutComponent,
    DocsaddComponent,
    DocslisteComponent,
    DocsdetailsComponent,
    DocsupdateComponent
  ],
  imports:  [
    FormsModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    RouterModule.forRoot([
      {path:"", component:HomeComponent, pathMatch: 'full'},
      {path:"ListesDesCandidats", component:ListesdescandidatsComponent},
      {path:"Listesdesarchivedescandidats", component:ListesArchivescandidatsComponent},
      {path:"Ajouteruncandidat", component:AddcandidatsComponent},
      {path:"Modifieruncandidat/:id", component:UpdatecandidatsComponent},
      {path:"Afficheruncandidat/:id", component:AfficheruncandidatsComponent},
      //{path:"Modifieruncandidat", component:UpdatecandidatsComponent},
      //{path:"Afficheruncandidat", component:AfficheruncandidatsComponent},
      {path:"Ajouterundocument", component:DocsaddComponent},
      {path:"Listedesdocuments", component:DocslisteComponent},
      {path:"Detailsdocument/:id", component:DocsdetailsComponent},
      {path:"Modifierundocument/:id", component:DocsupdateComponent},
      //{path:"Detailsdocument", component:DocsdetailsComponent},
      //{path:"Modifierundocument/", component:DocsupdateComponent},
      {path:"Deconnexion", component:LogoutComponent},
      {path:"**", component:ErreurComponent}
      
    ])
  ],
  providers: [CandidatsserviceService,DocumentsserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
