import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DocumentsserviceService } from '../../services/documentsservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-docsdetails',
  templateUrl: './docsdetails.component.html',
  styleUrls: ['./docsdetails.component.css']
})

export class DocsdetailsComponent implements OnInit {

document;

id = this.routeActive.snapshot.paramMap.get('id');

      constructor( 
	    private _http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private docservice:DocumentsserviceService,
    	private toastr:ToastrService
    	) { }

  ngOnInit() {
        this.docservice.getDocs(+this.id)
        .subscribe(success=>{
          this.document = success;
        })
  }

 ///////////////////get one par Id////////////////////////
		  getOneDocs(id){
		    this.docservice.getDocs(id)
		    .subscribe(success=>{
		      this.document = success;
		    })
		  }

}
