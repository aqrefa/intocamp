import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsdetailsComponent } from './docsdetails.component';

describe('DocsdetailsComponent', () => {
  let component: DocsdetailsComponent;
  let fixture: ComponentFixture<DocsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
