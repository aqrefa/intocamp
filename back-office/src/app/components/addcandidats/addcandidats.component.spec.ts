import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcandidatsComponent } from './addcandidats.component';

describe('AddcandidatsComponent', () => {
  let component: AddcandidatsComponent;
  let fixture: ComponentFixture<AddcandidatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcandidatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcandidatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
