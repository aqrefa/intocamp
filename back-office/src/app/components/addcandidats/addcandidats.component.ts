import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RouterModule, Routes,Router } from '@angular/router';
import { CandidatsserviceService } from '../../services/candidatsservice.service';

@Component({
  selector: 'app-addcandidats',
  templateUrl: './addcandidats.component.html',
  styleUrls: ['./addcandidats.component.css']
})
export class AddcandidatsComponent {

users;
isnewcreated;

//datecreationInput="01/01/2018";
//heureintegrationInput="09:00";
//passwordInput="123456";
//etatdossierInput="En cours";
//ajouterparInput="rh rh";
//loginInput="smezgani";

Currentday: number = Date.now();


  constructor(
    private candidatservice:CandidatsserviceService,
    private router:Router,
    private http:HttpClient, 
    private toastr:ToastrService) { }

  Envoiyerunmail(){
      
  }

  creationForm(form){
        this.http.post('http://127.0.0.1:8000/api/candidats',
        {civilite:form.value.civiliteInput,
        nom:form.value.nomInput,
        prenom:form.value.prenomInput,
        cin:form.value.cinInput,
        datenaissance:form.value.datenaissanceInput,
        situationfamiliale:form.value.sitauationfInput,
        profil:form.value.profilInput,
        disponibilite:form.value.disponibiliteInput,
        dateintegration:form.value.dateintegrationInput,
        heureintegration:form.value.heureintegrationInput,
        grade:form.value.gradeInput,
        numportable:form.value.portableInput,
        numfixe:form.value.fixeInput,
        email:form.value.emailInput,
        adresse:form.value.adresseInput,
        zonemobilite:form.value.zonemobiliteInput,
        commentaire:form.value.autreinfoInput,
        datecreation:form.value.datecreationInput,
        heurecreation:form.value.heurecreationInput,
        etatdossier:form.value.etatdossierInput,
        ajouterpar:form.value.ajouterparInput,
        password:form.value.passwordInput,
        login:form.value.loginInput,})
        .subscribe(success=>{
        this.toastr.success("Le colloborateur a été bien ajouté","Ajout")
        this.users=success;
        this.router.navigate(['ListesDesCandidats'])
  });
  }



}
