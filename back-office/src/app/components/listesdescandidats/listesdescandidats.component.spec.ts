import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListesdescandidatsComponent } from './listesdescandidats.component';

describe('ListesdescandidatsComponent', () => {
  let component: ListesdescandidatsComponent;
  let fixture: ComponentFixture<ListesdescandidatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListesdescandidatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListesdescandidatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
