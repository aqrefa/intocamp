import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { CandidatsserviceService } from '../../services/candidatsservice.service';
import { DataTablesModule } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';
import { RouterModule, Routes,Router } from '@angular/router';


@Component({
  selector: 'app-listesdescandidats',
  templateUrl: './listesdescandidats.component.html',
  styleUrls: ['./listesdescandidats.component.css']
})

export class ListesdescandidatsComponent implements OnInit {

users;

user;
id = this.routeActive.snapshot.paramMap.get('id');

etatdossierInputset="Archivé";
idInput;

      constructor( 
      	private router:Router,
	    private http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private candidatservice:CandidatsserviceService,
    	private toastr:ToastrService
    	) { }

	  ngOnInit() {
	  	this.getALLUsers();
	  }

///////////////////getALLUsers/////////////////////////
		getALLUsers(){
		    this.candidatservice.listdescandidats()
		    .subscribe(success=>{
		      this.users = success;
		    })
		}
///////////////////FIN getALLUsers/////////////////////////
///////////////////get one par Id////////////////////////
		   getOneUser(id){
		    this.candidatservice.getUser(id)
		    .subscribe(success=>{
		      this.users = success;
		    })
		  }
///////////////////fin get one par Id////////////////////////
///////////////////Delete by id///////////////////////
		  effacerUser(id){
		    this.candidatservice.deleteUser(id)
		    .subscribe(success=>{
		      //console.log("suppression OK");
		      this.toastr.success("le collaborateur a été bien suprimé","Suppression")
		      this.getALLUsers();
		    })
		  }
///////////////////fin Delete by id///////////////////////
/////////////Archiver un candidat///////////////////////
		  Archivercandidat(idInput){
		  	this.http.put('http://127.0.0.1:8000/api/candidats/'+idInput,
	         {etatdossier:this.etatdossierInputset})
	        .subscribe(success=>{
	        this.toastr.success("le collaborateur a été bien archivé","Archivage")
	        this.user=success;
	        this.router.navigate(['ListesDesCandidats'])
		    this.getALLUsers();
		   })
		   }
/////////////Fin Archiver un candidat///////////////////////

}


