import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocslisteComponent } from './docsliste.component';

describe('DocslisteComponent', () => {
  let component: DocslisteComponent;
  let fixture: ComponentFixture<DocslisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocslisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocslisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
