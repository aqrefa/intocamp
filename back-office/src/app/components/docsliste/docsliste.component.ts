import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { DocumentsserviceService } from '../../services/documentsservice.service';
import { DataTablesModule } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';
import { RouterModule, Routes,Router } from '@angular/router';


@Component({
  selector: 'app-docsliste',
  templateUrl: './docsliste.component.html',
  styleUrls: ['./docsliste.component.css']
})

export class DocslisteComponent implements OnInit {

documents;

document;

id = this.routeActive.snapshot.paramMap.get('id');

idInput;

      constructor( 
      	private router:Router,
	    private http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private docservice:DocumentsserviceService,
    	private toastr:ToastrService
    	) { }

	  ngOnInit() {
	  	this.getALLDocs();
	  }

///////////////////getALLDoces/////////////////////////
		getALLDocs(){
		    this.docservice.listdesdocs()
		    .subscribe(success=>{
		      this.documents = success;
		    })
		}
///////////////////FIN getALLDoces/////////////////////////
///////////////////get one par Id////////////////////////
		   getOneUser(id){
		    this.docservice.getDocs(id)
		    .subscribe(success=>{
		      this.documents = success;
		    })
		  }
///////////////////fin get one par Id////////////////////////
///////////////////Delete by id///////////////////////
		  effacerUser(idInput){
		    this.docservice.deleteDocs(idInput)
		    .subscribe(success=>{
		      //console.log("suppression OK");
		      this.toastr.success("le document a été bien suprimé","Suppression")
		      this.getALLDocs();
		    })
		  }
///////////////////fin Delete by id///////////////////////

}
