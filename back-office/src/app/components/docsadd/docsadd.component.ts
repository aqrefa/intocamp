import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RouterModule, Routes,Router } from '@angular/router';
import { DocumentsserviceService } from '../../services/documentsservice.service';


@Component({
  selector: 'app-docsadd',
  templateUrl: './docsadd.component.html',
  styleUrls: ['./docsadd.component.css']
})
export class DocsaddComponent {

docs;
isnewcreated;

Currentday: number = Date.now();

  constructor(
    private docservice:DocumentsserviceService,
    private router:Router,
    private http:HttpClient,
    private toastr:ToastrService) { }



  creationForm(form){
        this.http.post('http://127.0.0.1:8000/api/documents',
        {nomdoc:form.value.nomdocInput,
        sourcedoc:form.value.sourcedocInput,
        descdoc:form.value.descdocInput,
        datedoc:form.value.datedocInput,
        heuredoc:form.value.heuredocInput,
        ajouterpardoc:form.value.ajouterpardocInput,
        formatdoc:form.value.formatdocInput,
        icondoc:form.value.icondocInput,})
        .subscribe(success=>{
        this.toastr.success("Le document a été bien ajouté","Ajout")
        this.docs=success;
        this.router.navigate(['Listedesdocuments'])
  });
  }



}

