import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsaddComponent } from './docsadd.component';

describe('DocsaddComponent', () => {
  let component: DocsaddComponent;
  let fixture: ComponentFixture<DocsaddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocsaddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
