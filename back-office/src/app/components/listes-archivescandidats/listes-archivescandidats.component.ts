import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { CandidatsserviceService } from '../../services/candidatsservice.service';
import { DataTablesModule } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';
import { RouterModule, Routes,Router } from '@angular/router';

@Component({
  selector: 'app-listes-archivescandidats',
  templateUrl: './listes-archivescandidats.component.html',
  styleUrls: ['./listes-archivescandidats.component.css']
})
export class ListesArchivescandidatsComponent implements OnInit {


users;

user;
id = this.routeActive.snapshot.paramMap.get('id');

etatdossierInputset="En cours";
idInput;

      constructor( 
      	private router:Router,
	    private http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private candidatservice:CandidatsserviceService,
    	private toastr:ToastrService
    	) { }

	  ngOnInit() {
	  	this.getALLUsers();
	  }

///////////////////getALLUsers/////////////////////////
		getALLUsers(){
		    this.candidatservice.listdescandidats()
		    .subscribe(success=>{
		      this.users = success;
		    })
		}
///////////////////FIN getALLUsers/////////////////////////
///////////////////get one par Id////////////////////////
		   getOneUser(id){
		    this.candidatservice.getUser(id)
		    .subscribe(success=>{
		      this.users = success;
		    })
		  }
///////////////////Delete by id///////////////////////
		  effacerUser(id){
		    this.candidatservice.deleteUser(id)
		    .subscribe(success=>{
		      console.log("suppression OK");
		      this.toastr.success("le collaborateur a été bien suprimé","Suppression")
		      this.getALLUsers();
		    })
		  }
////////////////////////////////////
/////////////Désarchiver un candidat///////////////////////
		  Desarchivercandidat(idInput){
		  	this.http.put('http://127.0.0.1:8000/api/candidats/'+idInput,
	         {etatdossier:this.etatdossierInputset})
	        .subscribe(success=>{
	        this.toastr.success("le collaborateur a été bien désarchivé","Désarchivage")
	        this.user=success;
	        this.router.navigate(['Listesdesarchivedescandidats'])
		    this.getALLUsers();
		   })
		   }
/////////////Fin Désarchiver un candidat///////////////////////

}
