import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListesArchivescandidatsComponent } from './listes-archivescandidats.component';

describe('ListesArchivescandidatsComponent', () => {
  let component: ListesArchivescandidatsComponent;
  let fixture: ComponentFixture<ListesArchivescandidatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListesArchivescandidatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListesArchivescandidatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
