import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocsupdateComponent } from './docsupdate.component';

describe('DocsupdateComponent', () => {
  let component: DocsupdateComponent;
  let fixture: ComponentFixture<DocsupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocsupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocsupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
