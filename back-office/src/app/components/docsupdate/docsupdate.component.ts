import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RouterModule, Routes,Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { DocumentsserviceService } from '../../services/documentsservice.service';

@Component({
  selector: 'app-docsupdate',
  templateUrl: './docsupdate.component.html',
  styleUrls: ['./docsupdate.component.css']
})


export class DocsupdateComponent implements OnInit {

document;

id = this.routeActive.snapshot.paramMap.get('id');

isnewcreated;

sourcedocInput;

      constructor( 
	    private http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private docsservice:DocumentsserviceService,
    	private toastr:ToastrService,
    	private router:Router,
    	) { }


      ngOnInit(){
       ///////////////////get one par Id////////////////////////
  	       this.docsservice.getDocs(+this.id).subscribe(
           success=>{
           this.document=success;
               })
        ///////////////////fin get one par Id////////////////////////     	
      }


	  updateForm(form){
		    this.http.put('http://127.0.0.1:8000/api/documents/'+this.id,
	        {nomdoc:form.value.nomdocInput,
	        sourcedoc:form.value.sourcedocInput,
	        descdoc:form.value.descdocInput,
	        datedoc:form.value.datedocInput,
	        heuredoc:form.value.heuredocInput,
	        ajouterpardoc:form.value.ajouterpardocInput,
	        formatdoc:form.value.formatdocInput,
	        icondoc:form.value.icondocInput,})
	        .subscribe(success=>{
	        this.toastr.success("Le document a été bien modifier","Modification")
	        this.document=success;
	        this.router.navigate(['Listedesdocuments'])
	  });
	  }


}
