import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatecandidatsComponent } from './updatecandidats.component';

describe('UpdatecandidatsComponent', () => {
  let component: UpdatecandidatsComponent;
  let fixture: ComponentFixture<UpdatecandidatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatecandidatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatecandidatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
