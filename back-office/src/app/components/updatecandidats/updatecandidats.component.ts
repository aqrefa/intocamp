import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { RouterModule, Routes,Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CandidatsserviceService } from '../../services/candidatsservice.service';
import { DataTablesModule } from 'angular-datatables';

@Component({
  selector: 'app-updatecandidats',
  templateUrl: './updatecandidats.component.html',
  styleUrls: ['./updatecandidats.component.css']
})
export class UpdatecandidatsComponent implements OnInit {

user;
id = this.routeActive.snapshot.paramMap.get('id');

isnewcreated;

//datecreationInput="2018-10-04T12:07:30+00:00";
//passwordInput="123456";
//etatdossierInput="En cours";
//ajouterparInput="rh rh";
//loginInput="smezgani";

      constructor( 
	    private http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private candidatservice:CandidatsserviceService,
    	private toastr:ToastrService,
    	private router:Router,
    	) { }


      ngOnInit(){
       ///////////////////get one par Id////////////////////////
  	       this.candidatservice.getUser(+this.id).subscribe(
           success=>{
           this.user=success;
               })
        ///////////////////fin get one par Id////////////////////////     	
      }


	  updateForm(form){
	    this.http.put('http://127.0.0.1:8000/api/candidats/'+this.id,
	        {civilite:form.value.civiliteInput,
	        nom:form.value.nomInput,
	        prenom:form.value.prenomInput,
	        cin:form.value.cinInput,
	        datenaissance:form.value.datenaissanceInput,
	        situationfamiliale:form.value.sitauationfInput,
	        profil:form.value.profilInput,
	        disponibilite:form.value.disponibiliteInput,
	        dateintegration:form.value.dateintegrationInput,
	        heureintegration:form.value.heureintegrationInput,
	        grade:form.value.gradeInput,
	        numportable:form.value.portableInput,
	        numfixe:form.value.fixeInput,
	        email:form.value.emailInput,
	        adresse:form.value.adresseInput,
	        zonemobilite:form.value.zonemobiliteInput,
	        commentaire:form.value.autreinfoInput,
	        datecreation:form.value.datecreationInput,
	        heurecreation:form.value.heurecreationInput,
	        etatdossier:form.value.etatdossierInput,
	        ajouterpar:form.value.ajouterparInput,
	        password:form.value.passwordInput,
	        login:form.value.loginInput,})
	        .subscribe(success=>{
	        this.toastr.success("Le colloborateur a été bien modifier","Modification")
	        this.user=success;
	        this.router.navigate(['ListesDesCandidats'])
	  });
	  }


}
