import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { CandidatsserviceService } from '../../services/candidatsservice.service';
import { DataTablesModule } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-afficheruncandidats',
  templateUrl: './afficheruncandidats.component.html',
  styleUrls: ['./afficheruncandidats.component.css']
})
export class AfficheruncandidatsComponent implements OnInit {

user;
id = this.routeActive.snapshot.paramMap.get('id');

      constructor( 
	    private _http:HttpClient, 
	    private routeActive:ActivatedRoute,
    	private candidatservice:CandidatsserviceService,
    	private toastr:ToastrService
    	) { }

  ngOnInit() {
        this.candidatservice.getUser(+this.id)
        .subscribe(success=>{
          this.user = success;
        })
  }

 ///////////////////get one par Id////////////////////////
		  getOneUser(id){
		    this.candidatservice.getUser(id)
		    .subscribe(success=>{
		      this.user = success;
		    })
		  }

}
