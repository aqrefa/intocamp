import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AfficheruncandidatsComponent } from './afficheruncandidats.component';

describe('AfficheruncandidatsComponent', () => {
  let component: AfficheruncandidatsComponent;
  let fixture: ComponentFixture<AfficheruncandidatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AfficheruncandidatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AfficheruncandidatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
