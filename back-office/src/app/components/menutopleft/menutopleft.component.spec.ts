import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenutopleftComponent } from './menutopleft.component';

describe('MenutopleftComponent', () => {
  let component: MenutopleftComponent;
  let fixture: ComponentFixture<MenutopleftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenutopleftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenutopleftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
