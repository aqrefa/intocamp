-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 08, 2018 at 09:59 AM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intocapbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidats`
--

DROP TABLE IF EXISTS `candidats`;
CREATE TABLE IF NOT EXISTS `candidats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `datenaissance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `situationfamiliale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `disponibilite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateintegration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heureintegration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numportable` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numfixe` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zonemobilite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci,
  `datecreation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `heurecreation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etatdossier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ajouterpar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `candidats`
--

INSERT INTO `candidats` (`id`, `civilite`, `nom`, `prenom`, `cin`, `datenaissance`, `situationfamiliale`, `profil`, `disponibilite`, `dateintegration`, `heureintegration`, `grade`, `numportable`, `numfixe`, `email`, `adresse`, `zonemobilite`, `commentaire`, `datecreation`, `heurecreation`, `etatdossier`, `ajouterpar`, `password`, `login`) VALUES
(1, 'M', 'Mezgani', 'Said', 'AA17528', '31/03/1987', 'Célibataire', 'Ingénieur Angular', 'Immédiate', '09/09/2017', '09:00', 'A', '0616171422', '0537000000', 'mezgani.said@gmail.com', 'CYM', 'Rabat', 'Rien', '04/10/2018', '23:49', 'En cours', 'RANA', '123456', 'smezgani'),
(2, 'M', 'Mezgani', 'Said', 'AA17528', '31/03/1987', 'Marié', 'Ingénieur Angular', 'Immédiate', '09/09/2017', '09:00', 'A', '0616171422', '0537000000', 'mezgani.said@gmail.com', 'CYM', 'Rabat', 'Rien', '04/10/2018', '23:49', 'Archivé', 'RANA', '123456', 'smezgani'),
(3, 'Mme', 'Fassi', 'Saida', 'AA13438', '2000-01-01', 'Célibataire', 'Ingénieur .net', 'Aprés 2 mois', '09/09/2017', '09:00', 'C', '0600000000', '0500000000', 'mezgani.said@gmail.com', 'CYM', 'Casablanca', 'Rien', '04/10/2018', '23:49', 'En cours', 'KENZA', '123456', 'sfassi'),
(4, 'Mlle', 'Amrani', 'Asmae', 'RR4353', '1988-03-01', 'Marié', 'Ingénieur BI', 'Aprés 1 mois', '2018-12-12', '08:00', 'B', '0600000000', '0500000000', 'asmae@gmail.com', 'Takadoum', 'Casablanca', 'Rien', '06/10/2018', '12:51:10', 'En cours', 'KENZA', '123456', 'smezgani'),
(5, 'Mme', 'fatiha', 'Alaoui', 'EE43567', '2018-10-03', 'Divorcé', 'Ingénieur Qualité', 'Aprés 6 mois', '2018-10-28', '09:00', 'F', '0600000000', '0500000000', 'fatiha@yahoo.com', 'Rabat', 'Internationale', 'rien', '06/10/2018', '02:28:23', 'Archivé', 'KENZA', '123456', 'Alaouifatiha'),
(6, 'M', 'Qrefa', 'Adnane', 'EE43567', '1990-01-01', 'Célibataire', 'Ingénieur Symfony', 'Aprés 6 mois', '2015-01-01', '08:00', 'A', '0600000000', '0500000000', 'adnane@gmail.com', 'Rabat', 'Rabat', 'Rien', '07/10/2018', '01:44:09', 'En cours', 'KENZA', 'Cap123456', 'AQrefa'),
(7, 'Mme', 'Mesbahi', 'Houda', 'MM657774', '2003-01-29', 'Célibataire', 'Ingénieur Qualité', 'Aprés 3 mois', '2018-10-08', '09:00', 'A', '0600000000', '0500000000', 'houda@gmail.com', 'CYM', 'Rabat', 'Rien', '07/10/2018', '02:00:11', 'En cours', 'KENZA', 'Cap123456', 'hmezgani');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomdoc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sourcedoc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descdoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datedoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heuredoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ajouterpardoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formatdoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icondoc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `nomdoc`, `sourcedoc`, `descdoc`, `datedoc`, `heuredoc`, `ajouterpardoc`, `formatdoc`, `icondoc`) VALUES
(1, 'titre 1', 'chemin.ppp', 'Rien', '12/12/2018', '05:12:06', 'KENZA', '.ppp', 'icone ppp'),
(2, 'titre 2', 'chemin.pdf', 'Rien', '12/12/2018', '05:12:06', 'SELMA', '.pdf', 'icone pdf'),
(4, 'titre 3', 'chemin.pdf', 'Rien', '08/10/2018', '04:03:46', 'RANA', '.pdf', 'icone pdf'),
(5, 'titre 4', 'chemin.doc', 'Rien', '08/10/2018', '04:03:46', 'KENZA', '.doc', 'icone doc');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
